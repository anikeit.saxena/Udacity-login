import 'bootstrap/dist/css/bootstrap.min.css';
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { userService } from '../helper/user.service';

export default function Login() {
    const router = useRouter();

    const { register, handleSubmit, formState: { errors }} = useForm();

    const onSubmit = async (event) => {
    return userService
      .login(event.email, event.password)
      .then(() => {
        alert("Login successful", {
          keepAfterRouteChange: true,
        });
        const returnUrl = "/hello";
        router.push(returnUrl);
      })
      .catch(() => {
        alert("Invalid Credentials", {
          keepAfterRouteChange: true,
        });
      });
  };

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <input className={errors.email && errors.email.type === "required" ? 'form-control form-control-lg my-2 is-invalid' : 'form-control form-control-lg my-2'} id='regEmail' placeholder='Email Address' type='text' name='regEmail' style={{ fontSize: 16, padding: 14 }}
                        {...register("email", { required: true })}
                    />
                    {errors.email && errors.email.type === "required" &&
                        <div className='invalid-feedback'>Email Address is required</div>}
                </div>
                <div>
                    <div className='input-group'>
                        <input className={errors.password && errors.password.type === "required" ? 'form-control form-control-lg my-2 is-invalid' : 'form-control form-control-lg my-2'} id='regPassword' placeholder='Password' type='password' name='regPassword' style={{ fontSize: 16, padding: 14 }}
                            {...register("password", { required: true})}
                        />
                        {errors.password && errors.password.type === "required" &&
                            <div className='invalid-feedback'>Password is required</div>}
                    </div>
                </div>
                <div className='mb-4' style={{ fontSize: 14, color: 'rgb(109, 119, 128)' }}>
                    <span> By Clicking on Sign up, you agree to our <a href='https://www.udacity.com/legal/terms-of-service' style={{textDecoration: 'none', fontWeight: 500, color: '#017a9b'}}> Terms of Use </a> and our <a href='https://www.udacity.com/legal/privacy' style={{textDecoration: 'none', fontWeight: 500, color: '#017a9b'}}> Privacy Policy </a> </span>
                </div>
                <div className='col-md-4 text-center w-100'>
                    <button type='submit' className='btn btn-primary btn-lg shadow-sm p-2 rounded' style={{ backgroundColor: "rgb(1, 122, 155)" }}>
                        <p className='m-0 px-2 py-1' style={{ fontSize: 14, fontWeight: 500, letterSpacing: 2 }}>SIGN IN</p>
                    </button>
                </div>
            </form>

            <div className='container my-4'>
                <div className='d-flex flex-column justify-content-center'>

                    <div className='row justify-content-center'>
                        <div className='col-md-auto'>
                            <a href='https://www.udacity.com/legal/terms-of-service' style={{textDecoration: 'none', fontWeight: 500, color: '#017a9b'}}>Forgot your password?</a>
                        </div>
                    </div>

                    <div className='row justify-content-center p-0 my-4'>
                        <div className='col'><hr /></div>
                        <div className='col-md-auto p-0'>or</div>
                        <div className='col'><hr /></div>
                    </div>

                    <div className='row justify-content-center'>
                        <div className='col-md-auto'>
                            <a href='https://www.udacity.com/legal/terms-of-service' style={{textDecoration: 'none', fontWeight: 500, color: '#017a9b'}}>Sign in with your Organization</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}