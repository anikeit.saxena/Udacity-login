import Image from "next/image";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Facebook({ toggle }) {
    return (
        <div className='col-sm text-center'>
            <button type='button' className='btn btn-lg btn-block shadow-sm bg-white rounded ' style={{ width: '100%' }}>
                <div className='container d-flex p-0' style={{ justifyContent: 'space-between' }}>

                    <Image src='/facebook.svg' width={30} height={40} />

                    {toggle === 1 && <div className='align-self-center mx-1' style={{ fontSize: 14, fontWeight: 500, color: 'rgb(109, 119, 128)', alignSelf: 'center' }}>
                        Sign up with Facebook
                    </div>}
                    {toggle === 2 && <div className='align-self-center mx-1' style={{ fontSize: 14, fontWeight: 500, color: 'rgb(109, 119, 128)', alignSelf: 'center' }}>
                        Sign in with Facebook
                    </div>}
                </div>
            </button>
        </div>
    );
}