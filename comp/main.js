import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from 'react';
import Register from './registerform';
import Login from './loginform';
import Google from './google-button';
import Facebook from './facebook-button';

export default function Main() {

    const [toggleState, setToggleState] = useState(2);

    const toggleTab = (index) => {
        setToggleState(index);
    }

    return (
        <div className='container py-3 ' style={{ maxWidth: 960 }}>
            <div className='container p-0' style={{ maxWidth: 600, border: "1px solid rgb(200, 205, 210)" }}>

                {/* Header of Box containing Upper Buttons */}
                <ul className='nav nav-tabs nav-justified' style={{ color: "rgb(60, 90, 120)", background: "rgb(249, 249, 250)"}}>
                    <li className='nav-item'>
                        <button type='button' onClick={() => toggleTab(1)} className={toggleState === 1 ? "nav-link active" : "nav-link"} style={{ color: "rgb(60, 90, 120)", fontWeight: 500, height: 65}}>
                            SIGN UP
                        </button>
                    </li>
                    <li className='nav-item'>
                        <button type='button' onClick={() => toggleTab(2)} className={toggleState === 2 ? "nav-link active" : "nav-link"} style={{ color: "rgb(60, 90, 120)", fontWeight: 500, height: 65}}>
                            SIGN IN
                        </button>
                    </li>
                </ul>

                {/* Lower Body of the box */}
                <div className='tab-content'>

                    <div className='m-2 tab-pane active'>
                        <div className='container px-5 mt-5'>

                            {/* For Sign Up */}
                            <div>
                                {toggleState === 1 &&
                                    <h1 className='text-center' style={{ fontWeight: 100, marginBottom: 30 }}>
                                        Create your Account
                                    </h1>
                                }
                                {/* For Sign In */}
                                {toggleState === 2 &&
                                    <h1 className='text-center' style={{ fontWeight: 100, marginBottom: 30 }}>
                                        Sign in to your Account
                                    </h1>
                                }
                                <p className='text-center'>
                                    <span>
                                        Build skills for today, tomorrow, and beyond.
                                        <br />
                                        Education to future-proof your career.
                                    </span>
                                </p>
                            </div>


                            {/* Google and Facebook Buttons */}
                            <div className='container' style={{ marginTop: 30 }}>
                                <div className='row'>
                                    
                                    <Google toggle={toggleState} />

                                    <Facebook toggle={toggleState} />
                                    
                                </div>
                            </div>

                            {/*--------OR------ */}
                            <div className='container my-4'>
                                <div className='d-flex justify-content-center'>
                                    <div className='flex-fill bd-highlight'><hr /></div>
                                    <div className='bd-highlight px-4'>or</div>
                                    <div className='flex-fill bd-highlight'><hr /></div>
                                </div>
                            </div>

                            {/* Form for Register*/}
                            {toggleState === 1 && <Register />}

                            {/* Form for Signing in*/}
                            {toggleState === 2 && <Login />}

                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}